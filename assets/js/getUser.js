// in this js we can do view,edit and delete operation
var editor;
var table = null;
$('document').ready(function(e){
	loadUser();
function loadUser()
{
$('#userDetails').DataTable().destroy();
		$("#userDetails").show();
		 table3 =  $('#userDetails').DataTable( {
			"ajax": base_url+"admin/admin/getUserDetails",
				"columns": [ 
				    {"data": "firstname" },
					{"data": "lastname" },
					{"data": "email" },
					{"data": "gender" },
					{"data": "dob"},
					{"data": "primaryContact" },
					{"data": "city" },
					{"data": "role_id" },
					{"data": "status" },
					{"data": "creationDate"},
					{
						 "data": null,
						"defaultContent": '<button class="fa fa-pencil-square-o" id="editUser" data-toggle="modal" data-target=".bs-userDetails-modal-lg" value="" </button> <button class="fa fa-trash" id="deleteUser"></button> ' 	
					}
				]
		} );
}
// edit user		
$('#userDetails tbody').on( 'click', '#editUser', function (e)
		{
		var field = table3.row(  $(this).parents('tr') ).data();
		$("#id").val(field.id);
		$("#firstname").val(field.firstname);
		$("#lastname").val(field.lastname);
		$("#email").val(field.email);
		$("#gender").val(field.gender);
		$("#dob").val(field.dob);
		$("#primaryContact").val(field.primaryContact);
		$("#city").val(field.city);
		$("#role_id").val(field.role_id);
		$('.modal-body').find("#role_id").val(field.role_val);
		$('.modal-body').find("#status").val(field.status_val);
		$("#address").val(field.address);
		$("#state").val(field.state);
		$("#pincode").val(field.pincode);
		$("#country").val(field.country);
		$('#user').submit(function(e) {
			var updateData= $('#user').serializeArray();
			e.preventDefault();
		$.ajax({
			type : "GET",
			url : base_url + 'admin/admin/update_user',
			data : { 'user':updateData },
			dataType: "json",
			success: function(data) {
				 if (data.status == 1) {
					 $(".search-error").html(
								"<span style='color:green'>" + data.msg
										+ "</span>");
					  }
				 else
					 {
					 $(".search-error").html(
								"<span style='color:red'>"
										+ data.msg + "</span>");
					 }
			  },
			error : function(e) {
				console.log(e)
			},
			complete : function() {
			}
		});
		});	
		});		
// delete user
$('#userDetails tbody').on( 'click', '#deleteUser', function () {
	
	var data = table3.row(  $(this).parents('tr') ).data();
	alert( data.id);
		$("#id").val(data.id);
		var id = $("#id").val();	//alert(id);
			console.log(data.id); 
		$.ajax({
			  type: "POST",
			  url: base_url+'admin/admin/deleteUser',
			  data: {'user':data},
			  dataType: "json",
			  success: function(data) {
				  if (data.status == 1) {
				  window.location.href = base_url
					+ 'admin/admin/view_user';
			  }},
			  error: function(e){
				  console.log(e)
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			}); 
	
});
//hidden the bootstrap modal and load the following function
$('#myModal').on('hidden.bs.modal', function(){
	loadUser();
});
});