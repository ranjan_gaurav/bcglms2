// in this js we can do view,edit and delete privilege
var table = null;
$('document').ready( function(){
	loadPrivilege();


function loadPrivilege(){
	$('#privilegeDetails').DataTable().destroy();
		$("#privilegeDetails").show();
		 table1 =  $('#privilegeDetails').DataTable( {
			"ajax": base_url+"admin/admin/getPrivileges",
			  
				"columns": [
				 {"data": "id" },
				      {"data": "title" },
					{"data": "description" },
					{"data": "status"
					},
					
					{
						 "data": null,
						"defaultContent": '<button class="fa fa-pencil-square-o" id="editPrivilege" data-toggle="modal" data-target=".bs-privilegeDetails-modal-lg" value="" </button>  <button class="fa fa-trash" id="deletePrivilege"></button>' 
					}
				]
        	} );
}
// edit privilege
$('#privilegeDetails tbody').on( 'click', '#editPrivilege', function () {
		
		var editPrivilege = table1.row(  $(this).parents('tr') ).data();
		$("#id").val(editPrivilege.id);
		$("#title").val(editPrivilege.title);
		$("#description").val(editPrivilege.description);
		$('.modal-body').find("#status").val(editPrivilege.status_val); 
		
	    $('#privilege').submit(function(e) {
			var updateData= $('#privilege').serializeArray();
			e.preventDefault();
		$.ajax({
			type : "GET",
			url : base_url + 'admin/admin/update_privilege',
			data : { 'previlege':updateData },
			dataType: "json",
			success : function(data) {
				if (data.status == 1) {
					 $(".search-error").html(
								"<span style='color:green'>" + data.msg
										+ "</span>");
					  }
				 else
					 {
					 $(".search-error").html(
								"<span style='color:red'>"
										+ data.msg + "</span>");
					 }
			  },
			error : function(e) {
				console.log(e)
				},
			complete : function() {	
			}	
		});		
		});
});
// delete privilege
$('#privilegeDetails tbody').on( 'click', '#deletePrivilege', function () {
	
	var data = table1.row(  $(this).parents('tr') ).data();
		$("#id").val(data.id);
		var id = $("#id").val(); 
		$.ajax({
			  type: "POST",
			  url: base_url+'admin/admin/deletePrivilege',
			  data: {'user':data},
			  dataType: "json",
			  success: function(data) {
				  console.log(data.status);
				  if (data.status == 1) {
				  window.location.href = base_url
					+ 'admin/admin/view_privileges';
				 
				
			  }},
			  error: function(e){
				  console.log(e)
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			}); 
	});
//hidden the bootstrap modal and load the following function
$("#myModal").on("hidden.bs.modal", function () {
	loadPrivilege();
	});
});