 var newUserObj= {};
 $( document ).ready( function () {
 $( "#form" ).validate( {
			rules: {
				firstname: "required",
				lastname: "required",
				email: {
					required: true,
					email: true
				},
				username: {
					required: true,
					minlength: 4
				},
				password: {
					required: true,
					minlength: 5
				},
				primaryContact:{
					required: true,
					 phoneUS: true
				},
				
				status: "required",
				role: "required",
				picture: "required",
					gender:"required"
			},
			messages: {
				firstname: "Please enter your firstname",
				lastname: "Please enter your lastname",
				email: "Please enter a valid email address",
				username: {
					required: "Please enter a username",
					minlength: "Your username must consist of at least 4 characters"
				},
				password: {
					required: "Please provide a password",
					minlength: "Your password must be at least 5 characters long"
				},
				primaryContact:{
					required: "Please provide a primary contact number",
					minlength: "Your primary contact number must be at least 10 characters long"
					
				},
				status:"Please provide a status",
				role:"Please provide a role",
				picture: "Please provide a picture",
				gender:"please provide gender"
			},
			errorElement: "em",
			errorPlacement: function ( error, element ) {
				// Add the `help-block` class to the error element
				error.addClass( "help-block" );
				if ( element.prop( "type" ) === "checkbox"||element.prop( "type" ) === "radio" ) {
					error.insertAfter( element.parent( "div" ) );
				} else {
					error.insertAfter( element );
				}
			},
			highlight: function ( element, errorClass, validClass ) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-error" ).removeClass( "has-success" );
			},
			unhighlight: function (element, errorClass, validClass) {
				$( element ).parents( ".col-sm-5" ).addClass( "has-success" ).removeClass( "has-error" );
			},
			  submitHandler: UserDetails
		} );
function UserDetails()
{
	 newUserObj.firstname = $.trim($("#firstname").val());
		newUserObj.lastname = $.trim($("#lastname").val());
		newUserObj.username = $.trim($("#username").val());
		newUserObj.email = $.trim($("#email").val());
		newUserObj.password = $.trim($("#password").val());
		newUserObj.gender = $.trim($(".gender:checked").val());
		newUserObj.role_id = $.trim($(".role option:selected").val());
		newUserObj.status = $.trim($(".status:checked").val());
		newUserObj.dob = $.trim($("#dob").val());
		newUserObj.primaryContact = $.trim($("#primaryContact").val());
		newUserObj.address = $.trim($("#address").val());
		newUserObj.pincode = $.trim($("#pincode").val());
		newUserObj.city = $.trim($("#city").val());
		newUserObj.state = $.trim($("#state").val());
		newUserObj.country = $.trim($("#city").val());
		newUserObj.summary = $.trim($("#summary").val());
		if(newUserObj.role_id==3)
		{
		newUserObj.region = $.trim($("#district").val());
		
		}
		else if(newUserObj.role_id==2)
			{
			newUserObj.branch = $.trim($("#branch").val());
			//populateRoManger(newUserObj.branch);
			newUserObj.manager_id = $.trim($("#romanager").val());
			}else if(newUserObj.role_id==4)
			{
				
				newUserObj.manager_id = $.trim($("#branchmanger").val());
				}
			else if(newUserObj.role_id==5)
			{
				newUserObj.manager_id = $.trim($("#teller").val());
				}
		//console.log(newUserObj);
		$.ajax({
			  type: "POST",
			  url: base_url + 'admin/admin/new_user',
			  beforeSend : function(){
				  $('#loader').show();
			  },
			  data: {user: newUserObj},
			  dataType: "json",
			  success: function(data) {
				  console.log(data);
				  if(data.status == 1){
					  $.ajaxFileUpload({
							url : base_url + 'admin/admin/picture?userid='+data.userId,
							secureuri : false,
							fileElementId : 'fileToUpload',
							dataType : 'JSON',
							success : function(data) 
							{
							console.log(data);	
							}
						});	  
			  $('#search-error').html("<span style='color:green'>" +data.msg+"</span>");
			  $('form')[0].reset();
			  
		  }
		  else if(data.status == 0){
			  $('#search-error').html("<span style='color:red'>" +data.msg+"</span>");
			   $('form')[0].reset();
			  
		  }
		  else{
			  
			  $('#search-error').html("<span style='color:red'>Server error.</span>");
			  //$this.button('reset');
		  }
			  },
			  error: function(e){
				  console.log(e)
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			});
}
return false;
 });
 
 function populateRoManager(region_id)
 {
 	 //alert('hi');
 	if(region_id !=''){
         /*
             Pass your source type value to controller function which will return you json for your
             loaction dropdown
         */
         var url = base_url + 'admin/admin/region?branch_id='+ region_id; // 
         //console.log(data);
         $.get(url, function(data) {
        	// alert(data);
        	$("#romanager1").html(data)
         

     });
 	}
}