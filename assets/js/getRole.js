 // in this js we can do view,edit and delete of role
var table = null;
$('document').ready( function(){
	loadRoles();
function loadRoles(){
	$('#roleDetails').DataTable().destroy();
	$("#roleDetails").show();
	 table =  $('#roleDetails').DataTable( {
		"ajax": base_url+"admin/admin/getRoles",
		  
			"columns": [
			 {"data": "id" },
			      {"data": "title" },
				{"data": "description" },
				{"data": "status"},
				{
					 "data": null,
					"defaultContent": '<button class="fa fa-pencil-square-o" id="editRole" data-toggle="modal" data-target=".bs-roleDetails-modal-lg" value="" </button>  <button class="fa fa-trash" id="deleteRole"></button>'
				}
				
			]
    } );
}
// edit role 	
$('#roleDetails tbody').on( 'click', '#editRole', function (e) 
{
	$(".modal-body #privileges option:selected").prop("selected", false);
	$("#privileges").multiselect("refresh");
		var selectedPrv="";
		var message= [];
		var editRoleData = table.row(  $(this).parents('tr') ).data();
		$("#id").val(editRoleData.id);
		$("#title").val(editRoleData.title);
		$("#description").val(editRoleData.description); 
		$('.modal-body').find("#status").val(editRoleData.status_val); 
		if(editRoleData.roles_privilage.length>0){
			
			for(var i=0;i<editRoleData.roles_privilage.length;i++){
				
				var optionVal=editRoleData.roles_privilage[i].privilege_id;
				$('.modal-body #privileges [value='+optionVal+']').prop("selected", true);
				
			}
			$("#privileges").multiselect("refresh");
		}	
		$('#role').submit(function(e) {
			var updateData= $('#role').serializeArray();
			e.preventDefault();
			$.ajax({
			type : "GET",
			dataType: "json",
			url : base_url + 'admin/admin/update_role',
			data : { 'role':updateData },
			success: function(data) {
				 if (data.status == 1) {
					 $(".search-error").html(
								"<span style='color:green'>" + data.msg
										+ "</span>");
					  }
				 else
					 {
					 $(".search-error").html(
								"<span style='color:red'>"
										+ data.msg + "</span>");
					 }
			  },
			error : function(e) {
				console.log(e)
			},
			complete : function() {
			}
	   });	
     });
});

// delete role 
$('#roleDetails tbody').on( 'click', '#deleteRole', function () {
	
	var editRoleData = table.row(  $(this).parents('tr') ).data();
		$("#id").val(editRoleData.id);
		var id = $("#id").val();
			
		$.ajax({
			  type: "POST",
			  url: base_url+'admin/admin/deleteRole',
			  data: {'user':editRoleData},
			  dataType: "json",
			  success: function(data) {
				 if (data.status == 1) {
					 alert("your data deleted successfully");
						  window.location.href = base_url
							+ 'admin/admin/view_roles'; 
					  }
			  },
			  error: function(e){
				  console.log(e)
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			}); 
	
});
// hidden the bootstrap modal and load the following function
$("#myModal").on("hidden.bs.modal", function () {
	loadRoles();
	});
});
