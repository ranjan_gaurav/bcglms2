// in this js we can do view,edit and delete operation
var editor;
var table = null;
$('document').ready(function(e){
	loadLeads();
function loadLeads()
{
$('#leadDetails').DataTable().destroy();
		$("#leadDetails").show();
		 table4 =  $('#leadDetails').DataTable( {
			"ajax": base_url+"admin/admin/getLeadsDetails",
				"columns": [ 
				    {"data": "name" },
					{"data": "mobile" },
					{"data": "categoryName" },
					{"data": "productName" },
					{"data": "district"},
					{"data": "branch" },
					{"data": "added_name" },
					{"data": "emailId" },
					{"data": "leadValue" },
					{"data": "statusTitle"},
					{
						 "data": null,
						"defaultContent": ' <button class="fa fa-trash" id="deleteLead"></button> ' 	
					}
				]
		} );
}

// delete user
$('#leadDetails tbody').on( 'click', '#deleteLead', function () {
	
	var data = table4.row(  $(this).parents('tr') ).data();
	alert( data.id);
		$("#id").val(data.id);
		var id = $("#id").val();	//alert(id);
			console.log(data.id); 
		$.ajax({
			  type: "POST",
			  url: base_url+'admin/admin/deleteLead',
			  data: {'lead':data},
			  dataType: "json",
			  success: function(data) {
				  if (data.status == 1) {
				  window.location.href = base_url
					+ 'admin/admin/view_leads';
			  }},
			  error: function(e){
				  console.log(e)
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			}); 
	
});
//hidden the bootstrap modal and load the following function
/*$('#myModal').on('hidden.bs.modal', function(){
	loadLeads();
});*/
});