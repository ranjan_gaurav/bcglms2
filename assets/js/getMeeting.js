// in this js we can do view,edit and delete operation
var editor;
var table = null;
$('document').ready(function(e){
	loadMeetings();
function loadMeetings()
{
$('#meetingDetails').DataTable().destroy();
		$("#meetingDetails").show();
		 table3 =  $('#meetingDetails').DataTable( {
			"ajax": base_url+"admin/admin/getMeetingDetails",
				"columns": [ 
				    {"data": "title" },
					{"data": "description" },
					{"data": "user" },
					{"data": "leadName" },
					{"data": "address"},
					{"data": "Meeting_action_point" },
					{"data": "meetingTime" },
					{"data": "close_time" },
					{"data": "statusTitle"},
					{
						 "data": null,
						"defaultContent": '<button class="fa fa-pencil-square-o" id="editMeeting" data-toggle="modal" data-target=".bs-meetingDetails-modal-lg" value="" </button> <button class="fa fa-trash" id="deleteMeeting"></button> ' 	
					}
				]
		} );
}
// edit user		
$('#meetingDetails tbody').on( 'click', '#editMeeting', function (e)
		{
		var field = table3.row(  $(this).parents('tr') ).data();
		console.log(field);
		$("#id").val(field.id);
		$("#title").val(field.title);
		$("#description").val(field.description);
		$("#user").val(field.user);
		$("#leadId").val(field.leadName);
		$("#address").val(field.address);
		$("#meetingTime").val(field.meetingTime);
		$("#closeTiming").val(field.close_time);
		$('.modal-body').find("#status").val(field.statusTitle);
		$('#meeting').submit(function(e) {
			var updateData= $('#meeting').serializeArray();
			e.preventDefault();
		$.ajax({
			type : "GET",
			url : base_url + 'admin/admin/update_meetings',
			data : { 'user':updateData },
			dataType: "json",
			success: function(data) {
				 if (data.status == 1) {
					 $(".search-error").html(
								"<span style='color:green'>" + data.msg
										+ "</span>");
					  }
				 else
					 {
					 $(".search-error").html(
								"<span style='color:red'>"
										+ data.msg + "</span>");
					 }
			  },
			error : function(e) {
				console.log(e)
			},
			complete : function() {
			}
		});
		});	
		});		
// delete user
$('#meetingDetails tbody').on( 'click', '#deleteMeeting', function () {
	
	var data = table3.row(  $(this).parents('tr') ).data();
	alert( data.id);
		$("#id").val(data.id);
		var id = $("#id").val();	//alert(id);
			console.log(data.id); 
		$.ajax({
			  type: "POST",
			  url: base_url+'admin/admin/deleteUser',
			  data: {'user':data},
			  dataType: "json",
			  success: function(data) {
				  if (data.status == 1) {
				  window.location.href = base_url
					+ 'admin/admin/view_user';
			  }},
			  error: function(e){
				  console.log(e)
				  $('#loader').hide();
			  },
			  complete: function(){
				  $('#loader').hide();
			  }
			}); 
	
});
//hidden the bootstrap modal and load the following function
$('#myModal').on('hidden.bs.modal', function(){
	loadMeetings();
});
});