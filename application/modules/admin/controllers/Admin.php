<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' );
require APPPATH . "third_party/MX/Controller.php";
class Admin extends MX_Controller {
	public function __construct() {
		parent::__construct ();
		$this->load->model ( 'admin/Admin_service' );
		$this->load->helper('common');
		$this->load->library('excel');
		include_once './application/objects/Response.php';
	}
	public function index() {
		if (is_loggedin ()) {
			
			redirect ( 'admin/dashboard' );
		} else {
			$this->load->view ( 'admin/login' );
		}
	}
	public function dashboard() {
		if (is_loggedin ()) {
			$data ['metaData'] = 'yes';
			$data ['title'] = 'LMS | Dashboard';
			$data ['keywords'] = '';
			$data ['description'] = '';
			$this->template->load ( 'admin/dashboard', $data );
		} else {
			redirect ( 'admin' );
		}
	}
	public function forgot_password() {
		$this->load->view ( 'admin/forgot_password' );
	}
	public function reset_password() {
		$email = $this->input->post ( 'email' );
		
		$check = $this->Admin_service->checkEmail ( $email );
		
		if ($check) {
			
			$random_code = mt_rand ( 100000, 999999 ); // echo $random_code;
			$update = $this->Admin_service->updatePassword ( $random_code, $email );
			
			if ($update) {
				
				$response ['status'] = 1;
				$response ['msg'] = "Your new password has been sent to your email.";
			}
			
			$this->send_mail ( $random_code, $email );
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Email id doesn't exist";
		}
		
		echo json_encode ( $response );
	}
	function send_mail($random_code, $email) {
		error_reporting ( 0 );
		
		$message = "<b>Hi, </b>" . '</br>';
		$message .= '<b>Your new temporory password is ' . $random_code . ' </b>' . '</br>';
		$message .= '<b>After login you can change your password</b>' . '</br>';
		
		$this->load->library ( 'email' );
		$this->email->set_mailtype ( 'html' );
		$this->email->from ( 'contact@lms.com' );
		$this->email->to ( $email );
		$this->email->subject ( 'forgot Password' );
		$this->email->message ( $message );
		$this->email->send ();
	}
	public function profile() {
		$userid = $this->session->userdata ( 'id' );
		$data ['user'] = $this->Admin_service->GetUserProfile ( $userid );
		
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/profile', $data );
	}
	public function update_profile() {
		$data = $this->input->post ( 'user' );
		$userid = $this->session->userdata ( 'id' );
		$update = $this->Admin_service->update_profile ( $data, $userid );
		
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your Profile has been successfully updated";
		} 

		else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		
		echo json_encode ( $response );
	}
	public function change_password() {
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/change_password', $data );
	}
	public function update_password() {
		$data = $this->input->post ( 'user' );
		$userid = $this->session->userdata ( 'id' );
		if ($data ['password'] != '' || $data ['repassword'] != '') {
			if ($data ['password'] === $data ['repassword']) {
				unset ( $data ['repassword'] );
				$data ['password'] = md5 ( $data ['password'] );
				$update = $this->Admin_service->update_password ( $data, $userid );
				if ($update) {
					$response ['status'] = 1;
					$response ['msg'] = "Your Password has been successfully changed";
				}
			} 

			else {
				$response ['status'] = 0;
				$response ['msg'] = "Password doesn't match , please enter same password";
			}
		} 

		else {
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter the Password";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: view_roles
	 *         Description: view Roles
	 */
	public function view_roles() {
		$data ['detailPrivilege'] = $this->Admin_dao->detailPrivilege ();
		$data ['role'] = $this->Admin_service->getRoles ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_roles', $data );
	}
	public function getRoles() {
		$details ['data'] = $this->Admin_service->getRoles ();
		echo json_encode ( $details );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: view_privileges
	 *         Description: view privileges
	 */
	public function view_privileges() {
		$data ['user'] = $this->Admin_service->getPrivileges ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_privileges', $data );
	}
	public function getPrivileges() {
		$details ['data'] = $this->Admin_service->getPrivileges ();
		echo json_encode ( $details );
	}	 
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: updarte_role
	 *         Description: update role
	 */
	public function update_role() {
		$p=array();
		$previlege= array();
		$getUpdateRole = $_GET ['role'];
		$id = $getUpdateRole [0] ['value'];
		$title = $getUpdateRole [1] ['value'];
		$description = $getUpdateRole [2] ['value'];
		$status = $getUpdateRole [3] ['value'];
		for($i=4;$i<count($getUpdateRole);$i++)
		{
			$p['updateRolePrivilege']=$getUpdateRole[$i]['value'];
			array_push($previlege,$p);
		}
		$data = array (
				'id' => $id,
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		$update = $this->Admin_service->update_role ( $data,$previlege );
		
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully updated";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: update_privilege
	 *         Description: update privilege
	 */
	public function update_privilege() {
		$getUpdatePrivilege = $_GET ['previlege'];
		$id = $getUpdatePrivilege [0] ['value'];
		$title = $getUpdatePrivilege [1] ['value'];
		$description = $getUpdatePrivilege [2] ['value'];
		$status = $getUpdatePrivilege [3] ['value'];
		$data = array (
				'id' => $id,
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		$update = $this->Admin_service->update_privilege ( $data );
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your privilege has been successfully updated";
		} 
		else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *       @Date: 26th Nov 2016
	 *       @Method: add_role
	 *        @Description: add new role
	 */
	public function add_role() {
		$count ['detailPrivilege'] = $this->Admin_dao->detailPrivilege ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/add_role', $count );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_role
	 *         Description: add new role
	 */
	public function new_role() {
		$data = $this->input->post ( 'user' );
		
		if ($data ['title'] == "" || $data ['description'] == "" || $data ['status'] == "" || $data ['privilege'] == "") {
			
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter some data in mandatory field";
		} else {
			
			$insert = $this->Admin_service->new_role ( $data );
			if ($insert) {
				$response ['status'] = 1;
				$response ['msg'] = "You insert new role successfully";
			} 

			else {
				$response ['status'] = 0;
				$response ['msg'] = "Something went wrong , please try again";
			}
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: add_privilege
	 *         Description: add new privilege
	 */
	public function add_privilege() {
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/add_privilege', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 29th Nov 2016
	 *         Method: new_privilege
	 *         Description: add new privilege
	 */
	public function new_privilege() 

	{
		$data = $this->input->post ( 'user' );
		if ($data ['title'] == "" || $data ['description'] == "") 
		{
			
			$response ['status'] = 2;
			$response ['msg'] = "Please Enter the title or description";
		} else 
		{
			$insert = $this->Admin_service->new_privilege ( $data );
			if ($insert) {
				$response ['status'] = 1;
				$response ['msg'] = "You insert new privilege successfully";
			} 
			else 
			{
				$response ['status'] = 0;
				$response ['msg'] = "Something went wrong , please try again";
			}
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 30th Nov 2016
	 *         Method: add user
	 *         Description: add new user and this we also count how many role are present in role list
	 */
	public function add_user() 
	{
		$data ['countRole'] = $this->Admin_dao->countRole ();
		$data ['countBranchManager'] = $this->Admin_dao->countBranchManager ();
		$data ['countBranchManager'] = $this->Admin_dao->countBranchManager ();
		$data ['countTeller'] = $this->Admin_dao->countTeller ();
		$data ['branch'] = $this->Admin_dao->branch ();
		$data ['countDistrict'] = $this->Admin_dao->countDistrict ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		//print_R($data); die();
		$this->template->load ( 'admin/addUser', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 30 Nov 2016
	 *         Method: new_user
	 *         Description: add new user
	 */
	public function new_user() 

	{
		$trans_manager=array();
		//print_R($_POST); die();
		$response = array ();
		$data = $this->input->post ( 'user' );
//print_R($data); die();
		$trans_manager['manager_id']=$data['manager_id'];
		unset($data['manager_id']);
		$addUser = $this->Admin_service->new_user ( $data ,$trans_manager);

		if ($addUser != NULL) {
			$response ['status'] = 1;
			$response ['msg'] = "You insert new user successfully";
			$response ['userId'] = $addUser;
		} 
		else {
			$response ['status'] = 0;
			$response ['msg'] = "please fill the mandatory filled";
		}
		  
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 30 Nov 2016
	 *         Method: region
	 *         Description: according to region show ro manager in dropdown
	 */
public function region() 
	{
		$this->load->helper('form');
		$result=array();
		$region = $_GET['branch_id'];
		//print_R($region); die();
		$data  = $this->Admin_dao->countRoManager ($region);
		
		$view = '';
		$view .= '<select id="romanager" name="romanager" class="romanager" required> <option value="">Select Option</option>';
						foreach($data as $key=>$value){
							$view .= '<option value="'.$value['id'].'">'.$value['name'].'</option>';
							
							 
						}
		
		
			
						$view .= '</select>';
		

		echo $view;
		
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 1 Dec 2016
	 *         Method: picture
	 *         Description: picture
	 */
public function picture() {
		$userid = $_GET['userid'];
		$config ['upload_path'] = './uploads';
		$config ['allowed_types'] = 'gif|jpg|png';
		$config ['max_size'] = 1024;
		$config ['max_width'] = 1024;
		$config ['max_height'] = 768;
		$this->load->library ( 'upload', $config );
		$sizeImage = getimagesize ( $_FILES ['fileToUpload'] ['tmp_name'] );
		$width = $sizeImage [0];
		$height = $sizeImage [1];
		if ($width > 1024 && $height > 768) {
			$data = 'you upload size greater than allowed value';
			unlink ( $_FILES ['fileToUpload'] ['tmp_name'] );
			echo $data;
		} else {
			if (! $this->upload->do_upload ( 'fileToUpload' )) {
				$data = "somethning is going wrong";
			} else {
				$pic = array (
						'upload_data' => $this->upload->data () 
				);
			}
		}
		//base_url ( 'uploads' . $file ['upload_data'] ['file_name'] );
		$fileName=base_url ( 'uploads' . $pic ['upload_data'] ['file_name'] );
		//print_r($fileName); die;
		$insertPic = $this->Admin_dao->insert_pic ($userid, $fileName );
		//print_r($insertPic); die;
		if ($insertPic) {
			$response ['status'] = 1;
			$response ['msg'] = "You also insert new image successfully";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: view_user
	 *         Description: view existing user
	 */
	public function view_user()
	 {
		$data ['user'] = $this->Admin_service->getUserDetails ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_user', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: getUserDetails
	 *         Description: get all existing User Details
	 */
	public function getUserDetails() {
		$details ['data'] = $this->Admin_service->getUserDetails ();
		echo json_encode ( $details );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: view_image
	 *         Description:view existing image
	 */
	public function view_image() {
		$config = array ();
		$count = $this->Admin_dao->count ();
		$config ['base_url'] = base_url () . "admin/view_image";
		$config ['total_rows'] = $count;
		$config ['per_page'] = 8; // data showing perpage
		$config ["uri_segment"] = 3;
		$choice = $config ["total_rows"] / $config ["per_page"];
		$var = $config ['per_page'];
		$config ["num_links"] = round ( $choice );
		$this->pagination->initialize ( $config );
		$page = ($this->uri->segment ( 3 )) ? $this->uri->segment ( 3 ) : 0;
		$data ['files'] = $this->Admin_service->getImage ( $var, $page );
		$data ["links"] = $this->pagination->create_links ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_image', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: upload_image
	 *         Description:upload image
	 */
	public function upload_image() {
		$desc = $_REQUEST ['description'];
		$response = array ();
		$file_element_name = 'fileToUpload';
		$uploadPath = 'uploads/files/';
		$config ['upload_path'] = $uploadPath;
		$config ['allowed_types'] = 'gif|jpg|png';
		$this->load->library ( 'upload', $config );
		if (! $this->upload->do_upload ( $file_element_name )) {
			$response ['status'] = 2;
			$response ['msg'] = $this->upload->display_errors ( '', '' );
		} else {
			$data = $this->upload->data ();
			$file_id = $this->Admin_service->addImage ( $data ['file_name'], $desc );
			
			if ($file_id) {
				$response ['status'] = 1;
				$response ['msg'] = "File successfully uploaded";
			} else {
				unlink ( $data ['full_path'] );
				$response ['status'] = 0;
				$response ['msg'] = "Something went wrong when saving the file, please try again.";
			}
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: deletePrivilege
	 *         Description: delete existing Privilege means inactive the active Privilege
	 */
	public function deletePrivilege() {
		$data = $this->input->post ( 'user' );
		$id = $data ['id'];
		$delete = $this->Admin_service->deletePrivilege ( $id );
		if ($delete) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully deleted";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: deleteRole
	 *         Description: delete existing roles means inactive the active role
	 */
	public function deleteRole() {
		$data = $this->input->post ( 'user' );
		$id = $data ['id'];
		$delete = $this->Admin_service->deleteRole ( $id );
		if ($delete) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully deleted";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: deleteUser
	 *         Description: delete existing user means inactive the active user
	 */
	public function deleteUser() {
		$data = $this->input->post ( 'user' );
		$id = $data ['id'];
		$delete = $this->Admin_service->deleteUser ( $id );
		if ($delete) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully deleted";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *  @author : Nishant Singh
	 *  @Method: update_user
	 *  @Description: update existing user
	 */
	public function update_user() {
		$user= array();
		$updateDetails=array();
		$getUpdateUser = $_GET ['user'];
		for($i=0;$i<count($getUpdateUser);$i++)
		{
			$user['userUpadtedDetails']=$getUpdateUser[$i]['value'];
			array_push($updateDetails,$user);
		}
		$update = $this->Admin_service->update_user ( $updateDetails );
		if ($update) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully updated";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		
		echo json_encode ( $response );
	}

/**
	 *
	 * @author : Nishant Singh
	 *         Date: 6 April 2017
	 *         Method: add_leads
	 *         Description: add new leads in bulck
	 */
	public function add_leads() {
		
	
			
		if ($this->input->post ()) {
						
					$configUpload ['upload_path'] = './uploads/leads/';
					$configUpload ['allowed_types'] = '*';
					$configUpload ['file_name'] = md5(uniqid(rand(), true));
					$this->load->library ( 'upload', $configUpload );
					$this->upload->do_upload ( 'browse' );
					$upload_data = $this->upload->data ();
					
					if ($upload_data ['file_ext'] == '.xls' || $upload_data ['file_ext'] == '.xlsx') {
						$data ['hello'] = $upload_data;
						$file_name = $upload_data ['file_name']; // uploded file name
						$extension = $upload_data ['file_ext']; // uploded file extension
						$objPHPExcel = PHPExcel_IOFactory::load ( $_FILES ['browse'] ['tmp_name'] );
						// Tell PHPExcel that you will be loading a file.
						$objReader = PHPExcel_IOFactory::createReaderForFile ( $_FILES ['browse'] ['tmp_name'] );
						// Set your options.
						$objReader->setReadDataOnly ( true );
						$objPHPExcel = $objReader->load ( './uploads/leads/' . $file_name );
						$totalrows = $objPHPExcel->setActiveSheetIndex ( 0 )->getHighestRow ();
						$objWorksheet = $objPHPExcel->setActiveSheetIndex ( 0 );
						$allDataInSheet = $objPHPExcel->getActiveSheet ()->toArray (); // print_r( $allDataInSheet); die();
		
		
						$highestCol = $objPHPExcel->setActiveSheetIndex ( 0 )->getHighestColumn ();
						$highestRow = $objPHPExcel->setActiveSheetIndex ( 0 )->getHighestRow ();
						if (($highestCol == 'A') && ($highestRow == 1)) {
							
							$data ['Error_Message'] = '<div>Your Excel sheet is empty</div>';
						} else {
							
							for($i = 1; $i < $totalrows; $i ++) {
								//print_R($objWorksheet->getCellByColumnAndRow ( 1 )->getValue ()); die();
								if (($objWorksheet->getCellByColumnAndRow ( 0 )->getValue () != 'name') && ($objWorksheet->getCellByColumnAndRow ( 1 )->getValue () != 'mobile') && ($objWorksheet->getCellByColumnAndRow ( 2 )->getValue () != 'category_id') && ($objWorksheet->getCellByColumnAndRow ( 3 )->getValue () != 'product_id') && ($objWorksheet->getCellByColumnAndRow ( 4 )->getValue () != 'district') && ($objWorksheet->getCellByColumnAndRow ( 5 )->getValue () != 'branch') && ($objWorksheet->getCellByColumnAndRow ( 6 )->getValue () != 'sourcedBy') && ($objWorksheet->getCellByColumnAndRow ( 7)->getValue () != 'address') && ($objWorksheet->getCellByColumnAndRow ( 8 )->getValue () != 'email_id') && ($objWorksheet->getCellByColumnAndRow ( 9 )->getValue () != 'isExsitingCustomer') && ($objWorksheet->getCellByColumnAndRow ( 10 )->getValue () != 'leadValue') && ($objWorksheet->getCellByColumnAndRow ( 11 )->getValue () != 'cutomerId') && ($objWorksheet->getCellByColumnAndRow ( 12 )->getValue () != 'businessCard') && ($objWorksheet->getCellByColumnAndRow ( 13 )->getValue () != 'follower_id') && ($objWorksheet->getCellByColumnAndRow ( 14 )->getValue () != 'status') && ($objWorksheet->getCellByColumnAndRow ( 15 )->getValue () != 'is_assigned') && ($objWorksheet->getCellByColumnAndRow ( 16 )->getValue () != 'target_month') && ($objWorksheet->getCellByColumnAndRow ( 17 )->getValue () != 'note'))
								
								{
									// echo 123;
									$data ['Error_Message'] = '<div>Invalid template format.</div>';
								}
							else{
								if ($allDataInSheet [$i] [9] == '1' ) {
									$custId=$allDataInSheet [$i] [11];
									$note=$allDataInSheet [$i] [17];
								}else{
									$custId=" ";
									$note=" ";
								}
								if($this->session->userdata('logged_in')){
								
									$added_by= $this->session->userdata('id');
								}
										$insertdata = array (
												'name' => $allDataInSheet [$i] [0],
												'mobile' => $allDataInSheet [$i] [1],
												'category_id' => $allDataInSheet [$i] [2],
												'product_id' => $allDataInSheet [$i] [3],
												'district' => $allDataInSheet [$i] [4],
												'branch' => $allDataInSheet [$i] [5],
												'sourcedBy' => $allDataInSheet [$i] [6],
												'address' => $allDataInSheet [$i] [7],
												'emailId' => $allDataInSheet [$i] [8],
												'isExistingCustomer' =>$allDataInSheet [$i] [9],
												'leadValue' =>$allDataInSheet [$i] [10],
												'cutomerId' =>$custId,
												'businessCard'=>$allDataInSheet [$i] [12],
												'follower_id'=>$allDataInSheet [$i] [13],
												'status'=>$allDataInSheet [$i] [14],
												'is_assigned'=>$allDataInSheet [$i] [15],
												'target_month'=>$allDataInSheet [$i] [16],
												'note'=>$note,
												'added_by'=>$added_by
												
										);
										
										$inserted = $this->Admin_dao->insertLeadData ( $insertdata );
										if ($inserted) {
											$data ['upload_message'] = '<div>Data has been Uploaded.</div>';
										} else {
											$data ['upload_message'] = '<div>Something went wrong , please try again.</div>';
										}
									
								
							}
						}
						}
						unlink('./uploads/leads/'.$configUpload ['file_name'].$upload_data ['file_ext']);
						
		}else {
				$data ['error'] = 'File not found';
			}
		}	 
			$data ['metaData'] = 'yes';
			$data ['title'] = 'LMS | Dashboard';
			$data ['keywords'] = '';
			$data ['description'] = '';
			
			//print_R($data); die();
			$this->template->load ( 'admin/add_leads', $data );
	}
		
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: view_user
	 *         Description: view existing user
	 */
	public function view_leads()
	{
		$data ['leads'] = $this->Admin_service->getLeadsDetails ();
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		$this->template->load ( 'admin/view_leads', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: getLeadsDetails
	 *         Description: get all existing Lead Details
	 */
	public function getLeadsDetails() {
		$details ['data'] = $this->Admin_service->getLeadsDetails ();
		echo json_encode ( $details );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: deleteLead
	 *         Description: delete existing lead means inactive the active lead
	 */
	public function deleteLead() {
		$data = $this->input->post ( 'lead' );
		$id = $data ['id'];
		$delete = $this->Admin_service->deleteLead ( $id );
		if ($delete) {
			$response ['status'] = 1;
			$response ['msg'] = "Your role has been successfully deleted";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: view_meeting
	 *         Description: view existing meeting
	 */
	public function view_meetings()
	{    $id= $this->session->userdata('id');
		$data ['meetings'] = $this->Admin_service->getMeetingDetails ($id);
		$data ['metaData'] = 'yes';
		$data ['title'] = 'LMS | Dashboard';
		$data ['keywords'] = '';
		$data ['description'] = '';
		//print_R($data); die();
		$this->template->load ( 'admin/view_meetings', $data );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: getLeadsDetails
	 *         Description: get all existing Lead Details
	 */
	public function getMeetingDetails() {
		$id= $this->session->userdata('id');
		$details ['data'] = $this->Admin_service->getMeetingDetails ($id);
		//print_R(json_encode ( $details )); die();
		echo json_encode ( $details );
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Method: deleteLead
	 *         Description: delete existing lead means inactive the active lead
	 */
	public function deleteMeeting() {
		$data = $this->input->post ( 'lead' );
		$id = $data ['id'];
		$delete = $this->Admin_service->deleteMeeting ( $id );
		if ($delete) {
			$response ['status'] = 1;
			$response ['msg'] = "Your meeting has been successfully deleted";
		} else {
			$response ['status'] = 0;
			$response ['msg'] = "Something went wrong , please try again";
		}
		echo json_encode ( $response );
	}
}