<?php
/**
 * @author : Anjani Kr. Gupta
 * @date: 10th Nov 2016
 * Description : This Dao class is responsible of all the database related operation for API
 */
class Admin_dao extends CI_Model {
	public function __construct() {
		parent::__construct ();
		include_once './application/objects/Response.php';
		date_default_timezone_set('Asia/Calcutta');
		$this->load->helper('date');
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: checkEmail
	 *         Description: check emalid is present or not in our database
	 */
	public function checkEmail($email) {
		$this->db->where ( 'email', $email );
		$result = $this->db->get ( 'user' );
		$rows = $result->num_rows ();
		//print_R($rows); die();
		if ($rows === 1) {
			return true;
		}
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: updatePassword
	 *         Description: update the password
	 */
	public function updatePassword($random_code, $email) {
		$data ['password'] = md5 ( $random_code );
		$this->db->where ( 'email', $email );
		$update = $this->db->update ( 'user', $data );
		
		if ($update) {
			return true;
		} 

		else {
			return false;
		}
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: GetUserProfile
	 *         Description: get the profile of user
	 */
	public function GetUserProfile($userid) {
		$this->db->where ( 'user.id', $userid );
		$this->db->select('user.*,m_roles.title as roleTitle');
		$this->db->from('user');
		$this->db->join('m_roles','user.role_id=m_roles.id','left');
		$result = $this->db->get ( );
		$rows = $result->row ();
		//print_R($rows); die();
		return $rows;
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: update_profile
	 *         Description: update the profile of the user
	 */
	public function update_profile($data, $userid) {
		$this->db->where ( 'id', $userid );
		$result = $this->db->update ( 'user', $data );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Anjani Kumar Gupta
	 *         Date: 10th Nov 2016
	 *         Method: update_password
	 *         Description: update the password by the registered user
	 */
	public function update_password($data, $userid) {
		$this->db->where ( 'id', $userid );
		$result = $this->db->update ( 'user', $data );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: getRoles
	 *         Description: get Roles
	 */
	public function getRoles() {
		$role = array();
		$r = array();
		$q = "select id,
					title,
					description, 
					status as status_val, 
					(select title from m_status where m_status.id=m_roles.status) as status  
					from m_roles";
		$query = $this->db->query ( $q );
		if($query->num_rows()>0){
			foreach ($query->result() as $row){
				$r['id'] = $row->id;
				$r['title'] = $row->title;
				$r['description'] = $row->description;
				$r['status'] = $row->status;
				$r['status_val'] = $row->status_val;
				$r['roles_privilage'] = $this->getRolesPrivilege($row->id);
				array_push($role,$r);
			}
			//echo'<pre>';	print_r($role); die();
		}
		return $role;
	}
	
	private function  getRolesPrivilege($roleId){
		$q = "SELECT   t.privilege_id  
				from trans_role_privileges as t
				where t.role_id=".$roleId;
		$query = $this->db->query ( $q );		
		return $query->result_array();
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: getPrivileges
	 *         Description: get Privileges
	 */
	public function getPrivileges() 

	{
		$query = $this->db->query ( "select id,
				title,
				description, 
				status as status_val,
				(select title from m_status where m_status.id=m_privileges.status) as status
				 from m_privileges" );
		$result = $query->result_array ();
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: update_role
	 *         Description: update the existing role
	 */
	public function update_role($data, $previlege) {
		$updatedPrivilege = array ();
		$updateprev = array ();
		for($i = 0; $i < count ( $previlege ); $i ++) {
			$updateprev ['updatedRolePrev'] = $previlege [$i] ['updateRolePrivilege'];
			array_push ( $updatedPrivilege, $updateprev );
		}
		$id = $data ['id'];
		$title = $data ['title'];
		$description = $data ['description'];
		$status = $data ['status'];
		$value = array (
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'm_roles', $value );
		
		if ($id) {
			$delete = $this->db->query ( "delete from trans_role_privileges where role_id=" . $this->db->escape ( $id ) );
			if ($delete) {
				for($i = 0; $i < count ( $updatedPrivilege ); $i ++) {
					
					$transPrivilege = array (
							'role_id' => $id,
							'privilege_id' => $updatedPrivilege [$i] ['updatedRolePrev'] 
					);
					$trans_privilege = $this->db->insert ( 'trans_role_privileges', $transPrivilege );
				}
			}
		}
		return $trans_privilege;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: update_privilege
	 *         Description: update the existing Privileges
	 */
	public function update_privilege($data) {
		$id = $data ['id'];
		$title = $data ['title'];
		$description = $data ['description'];
		$status = $data ['status'];
		
		$value = array (
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'm_privileges', $value );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_role
	 *         Description:Add new Role
	 */
	public function new_role($data) {
		$title = $data ['title'];
		$description = $data ['description'];
		$status = $data ['status'];
		$privilege = $data ['privilege'];
		$count = count ( $privilege );
		
		$roleData = array (
				'title' => $title,
				'description' => $description,
				'status' => $status 
		);
		
		$role = $this->db->insert ( 'm_roles', $roleData );
		if ($role) {
			$roleId = $this->db->insert_id ();
		}
		if (! empty ( $roleId )) {
			for($i = 0; $i <= $count; $i ++) {
				$transPrivilege = array (
						'role_id' => $roleId,
						'privilege_id' => $privilege [$i] 
				);
				
				$trans_privilege = $this->db->insert ( 'trans_role_privileges', $transPrivilege );
			}
		}
		return true;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_privilege
	 *         Description: add new Privileges
	 */
	public function new_privilege($data) {
		$this->db->insert ( 'm_privileges', $data );
		return true;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_user
	 *         Description:add new user
	 */
	public function new_user($data,$trans_manager) {
	//print_R($data); die();
		$data['password']=md5($data['password']);
		
		$userId = NULL;
		$user = $this->db->insert ( 'user', $data );
	//	print_R($data); die();
		if ($user) {
			$userId = $this->db->insert_id ();
		}
		if($userId)
		{
			$trans_user=array('user_id'=>$userId);
			if($trans_manager['manager_id']!="")
			{
				$trans_user['manager_id']= $trans_manager['manager_id'];
			}
			else{
				$trans_user['manager_id']= NULL;
			}
		if($data['role_id']=='3'||$data['role_id']=='4'||$data['role_id']=='5')
		{
			$trans_user_manager = $this->db->insert ( 'trans_user_manager', $trans_user );
		}
		}
		return $userId;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: insert_pic
	 *         @Description: adding image into user
	 */
	public function insert_pic($id , $filename) {
		//$filename=base_url('uploads/'.$filename);
		$sql = " update user set picture=".$this->db->escape($filename)." where id=".$this->db->escape($id);
		//print_r($sql); die();
		$result = $this->db->query ( $sql );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: getUserDetails
	 *         Description:geting existing User Details
	 */
	public function getUserDetails() {
		$query = $this->db->query ( "SELECT id, 
				firstname, 
				lastname, 
				email, 
				gender, 
				dob,  
				primaryContact, 
                 city,
				role_id as role_val,
				status as status_val,
				(select title from m_roles where m_roles.id=user.role_id) as role_id, 
				(select title from m_status where m_status.id=user.status) as status, 
                creationDate,
				address,
				pincode,
				state,
				country
				 FROM user" );
		$result = $query->result_array ();
		return $result;
	}
	
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: addImage
	 *         @Description: adding image into gallery
	 * @param
	 *        	eters: filename,description
	 */
	public function addImage($filename, $description) {
		$sql = "INSERT INTO gallery(`picture`,
				`description`,
				`added_by`) 
				VALUES(" . $this->db->escape ( $filename ) . 
		"," . $this->db->escape ( $description ) . 
		",305)";
		$result = $this->db->query ( $sql );
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 * @Method: count
	 * @Description: count how many picture in gallery
	 */
	public function count() {
		$count = $this->db->get ( 'gallery' )->num_rows ();
		return $count;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: getImage
	 *         @Description: get image from database
	 * @param
	 *        	eters: limit, start
	 */
	public function getImage($limit, $start) {
		$this->db->select ( 'picture,description' );
		$this->db->limit ( $limit, $start );
		$this->db->order_by ( 'creationDate', 'desc' );
		$query = $this->db->get ( 'gallery' );
		if ($query->num_rows () > 0) {
			foreach ( $query->result () as $row ) {
				$data [] = $row;
			}
			return $data;
		}
		return false;
	}
	/**
	 *
	 * @author : Nishant Singh
	 * @Method: deletePrivilege
	 * @Description: deleted privileges from database . it actually inactive the privileges and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deletePrivilege($id) {
		$value = array (
				'status' => '2' 
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'm_privileges', $value );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 * @Method: deleteRole
	 * @Description: deleted role from database . it actually inactive the roles and kept in list as inactive
	 * @parameters: id
	 */
	public function deleteRole($id) {
		$value = array (
				'status' => '2' 
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'm_roles', $value );
		
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: deleteUser
	 *         @Description: deleted user from database . it actually inactive the user and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deleteUser($id) 
	   {
		$value = array (
				'status' => '2' 
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'user', $value );
		if ($result)
		 {
			return true;
		 }
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: detailPrivilege
	 *         @Description:count how many privilege are in list .
	 */
	public function detailPrivilege()
	 {
		$this->db->select ( 'id,title' );
		$this->db->where ( 'status', ACTIVE );
		$count = $this->db->get ( 'm_privileges' );
		$result = $count->result ();
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: countRole
	 *         @Description:count how many role are in list .
	 */
	public function countRole() 
	{
		$this->db->select ( 'id,title' );
		
		$count = $this->db->get ( 'm_roles' );
		$result = $count->result ();
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: branch
	 *         @Description:count how many branch are in list .
	 */
	public function branch()
	{
		$this->db->select ( 'id,title' );
	
		$count = $this->db->get ( 'm_branch' );
		$result = $count->result ();
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: countRoManager
	 *         @Description:count how many Ro-Manager are in list .
	 */
	public function countRoManager($region)
	{
		$data=array();
		$q=array();
		$this->db->select('district_id');
		$this->db->where('id',$region);
		$district = $this->db->get ( 'm_branch' );
		$district_id = $district->result();
		//print_R($district_id); die();
		foreach($district_id as $district)
			//print_R($district->district_id); die();
		if($district_id)
		{
		$this->db->select ( 'id,firstname,lastname' );
	$this->db->where('role_id','2');
	$this->db->where('region',$district->district_id);
		$count = $this->db->get ( 'user' );
		$result = $count->result_array();
		//print_R(count($result));die();
		  for($i=0;$i<count($result);$i++)
		{
		$q['id']=$result[$i]['id'];
		$q['name']=$result[$i]['firstname']." ".$result[$i]['lastname'];	
		array_push($data,$q);
		}  
		return $data;
		}
		return false;
		
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: countRoManager
	 *         @Description:count how many Ro-Manager are in list .
	 */
	public function countBranchManager()
	{
		$this->db->select ( 'id,firstname,lastname' );
		$this->db->where('role_id','3');
		$count = $this->db->get ( 'user' );
		$result = $count->result ();
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: countRoManager
	 *         @Description:count how many Ro-Manager are in list .
	 */
	public function countTeller()
	{
		$this->db->select ( 'id,firstname,lastname' );
		$this->db->where('role_id','4');
		$count = $this->db->get ( 'user' );
		$result = $count->result ();
		return $result;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         @Method: countDistrict
	 *         @Description:count how many District are in list .
	 */
	public function countDistrict()
	{
		$this->db->select ( 'id,title' );
		$count = $this->db->get ( 'm_district' );
		$result = $count->result ();
		return $result;
	}
/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: update_user
	 *         Description: update the existing user
	 */
	public function update_user($updateDetails) 
	    {
		 $id = $updateDetails ['0']['userUpadtedDetails'];
		$firstName =  $updateDetails ['1']['userUpadtedDetails'];
		$lastName = $updateDetails ['2']['userUpadtedDetails'];
		$email = $updateDetails ['3']['userUpadtedDetails'];
		$gender = $updateDetails ['4']['userUpadtedDetails'];
		$dob =$updateDetails ['5']['userUpadtedDetails'];
		$primaryContact = $updateDetails ['6']['userUpadtedDetails'];
		$address= $updateDetails ['7']['userUpadtedDetails'];
		$pincode=$updateDetails ['8']['userUpadtedDetails'];
		$city=$updateDetails ['9']['userUpadtedDetails'];
		$state=$updateDetails ['10']['userUpadtedDetails'];
		$country = $updateDetails ['11']['userUpadtedDetails'];
		$role= $updateDetails ['12']['userUpadtedDetails'];
		$status= $updateDetails ['13']['userUpadtedDetails'];
		$value = array (
				'firstname' => $firstName,
				'lastname' => $lastName,
				'email' => $email ,
				'gender' => $gender,
				'dob' => $dob,
				'primaryContact' => $primaryContact,
				'address' => $address,
				'pincode' => $pincode,
				'city' => $city,
				'state' => $state,
				'country' => $country,
				'role_id' => $role,
				'status' => $status,
				'modificationDate'=> date('Y-m-d H:i:s',now())
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'user', $value );
	if ($result)
	  {
			return true;
	  }
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: new_privilege
	 *         Description: add new Privileges
	 */
	public function insertLeadData($data) {
		
		//print_R($data);die();
		$this->db->insert ( 'leads', $data );
		return true;
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: getLeadsDetails
	 *         Description: see  Leads Details
	 */
	public function getLeadsDetails() {
	
		
       $this->db->select('leads.*,m_status.title as statusTitle,CONCAT(user.firstname," ",user.lastname) as added_name,m_category.category as categoryName,m_district.title as district,m_branch.title as branch,category_products.product as productName',false);
        $this->db->from('leads');
        $this->db->join('user','leads.added_by=user.id','left');
        $this->db->join('m_status','leads.status=m_status.id','left');
        $this->db->join('m_category','leads.added_by=user.id','left');
        $this->db->join('m_district','leads.district=m_district.id','left');
        $this->db->join('m_branch','leads.branch=m_branch.id','left');
        $this->db->join('category_products','leads.product_id=category_products.id','left');
        $res = $this->db->get();
        //print_R($res->result()); die();
        return $res->result();
	}
	/**
	 *
	 * @author : Nishant Singh
	 * @Method: deletePrivilege
	 * @Description: deleted privileges from database . it actually inactive the privileges and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deleteLead($id) {
		$value = array (
				'status' => '2'
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'leads', $value );
		if ($result) {
			return true;
		}
	}
	/**
	 *
	 * @author : Nishant Singh
	 *         Date: 26th Nov 2016
	 *         Method: getLeadsDetails
	 *         Description: see  Leads Details
	 */
	public function getMeetingDetails($id) {
	
	//print_R($id); die();
		
	$this->db->select('meetings.*,m_status.title as statusTitle,CONCAT(user.firstname," ",user.lastname) as user,leads.name as leadName',false);
	$this->db->where('meetings.user_id',$id);	
	$this->db->from('meetings');
		$this->db->join('m_status','meetings.status=m_status.id','left');
		$this->db->join('user','meetings.user_id=user.id','left');
		$this->db->join('leads','meetings.lead_id=leads.id','left');
		$res = $this->db->get();
		//print_R($res->result()); die();
		return $res->result();
	}
	/**
	 *
	 * @author : Nishant Singh
	 * @Method: deleteMeeting
	 * @Description: deleted Meeting from database . it actually inactive the Meeting and kept in list as inactive
	 * @param
	 *        	eters: id
	 */
	public function deleteMeeting($id) {
		$value = array (
				'status' => '2'
		);
		$this->db->where ( 'id', $id );
		$result = $this->db->update ( 'meetings', $value );
		if ($result) {
			return true;
		}
	}
}