<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
          <?php $this->view("includes/side_menu"); ?>
        </div>
		 <?php $this->view("includes/top_nav"); ?>
				<!-- page content -->

			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>Upload Excel</h2>
									<div class="clearfix"></div>
								</div>
												<div class="row">
													<div class="col-md-3 col-sm-6 col-xs-12">

														<form enctype="multipart/form-data" action=""
															method="post" id="image">
															
															<div class="form-group">
																<label>Choose Files</label> <input type="file"
																	class="form-control" name="userfile"
																	id="userfile" />
															</div>
															<div class="form-group">
																<input class="form-control" type="button"
																	name="fileSubmit" id="submit" value="UPLOAD" />
															</div>
														</form>
													</div>
												</div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					</body>
