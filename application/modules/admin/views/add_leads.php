 <?php //print_r($error); die(); ?>
<meta HTTP-EQUIV="Pragma" content="no-cache">
<meta HTTP-EQUIV="Expires" content="-1">
<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
         <?php $this->view("includes/side_menu"); ?>
        </div>
		 <?php $this->view("includes/top_nav"); ?>
        <!-- page content -->
			<div class="right_col" role="main">
				<!-- page content -->


				<div class="row">
					<div class="col-md-12 col-sm-12 col-xs-12">
						<div class="x_panel">
 <div class="x_title">
                    <h2><b>Add Leads</b></h2>
                   <div class="x_content">
                   <?php 
                   if(isset($error) ){
                   	echo '<div class="alert alert-error alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>'.$error.'</div>';
                   }
               if (isset($upload_message)) {
								echo '<div class="alert alert-success alert-dismissable"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Success!</strong>'.$upload_message.'</div>';
							}
							if (isset($Error_Message)) {
								// echo $Error_Message;
								echo '<div class="alert alert-error alert-dismissible"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Error!</strong>' . $Error_Message . '</div>';
							}	
							?>
						<form id="leads_form" method="post" data-toggle="validator"
											class="form-horizontal form-label-left required"
											enctype="multipart/form-data">
						<div class="row">
							    <div class="col-sm-3"><h4 class="m0">Upload Data</h4></div>	
							<div class="col-sm-3"><input type="file" name="browse" id="browse" /><div style="font-size: 11px; top: 3px; font-style: normal; text-align: left; left: 4px; position: relative;">Only .xls file or .xlsx is allowed</div></div>
							<div class="col-sm-6"><input type="submit" value="Upload" name="submit_file" class="btn btn-primary pull-right" /></div>
							</div>
						</form>
					</div>
                    <div class="clearfix"></div>
                  </div>
</div></div></div></div></div></div></body>