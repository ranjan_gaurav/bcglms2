<body class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
         <?php $this->view("includes/side_menu"); ?>
        </div>
		 <?php $this->view("includes/top_nav"); ?>
				<!-- page content -->

			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 item form-group pull-right top_search">
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>View Meetings Details</h2>
									<div class="clearfix"></div>
								</div>
								<div class="x_content">
									<table id="meetingDetails"
										class="display table table-striped table-bordered no-footer dataTable"
										cellspacing="0" width="100%">
										<thead>
											<tr>
											
												<th>Title</th>
												<th>Description</th>
												<th>User</th>
												<th>Lead</th>
												<th>Address</th>
												<th>Meeting_action_point</th>
												<th>Meeting Time</th>
												<th>Close Time</th>
												<th>Status</th>
												<th>Action</th>
											</tr>
										</thead>
									</table>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="modal fade bs-meetingDetails-modal-lg" tabindex="-1" id="myModal"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">Edit Meeting</h4>
						</div>
						<div class="modal-body">
							<form method="post" id="meeting"
								class="form-horizontal form-label-left" novalidate>


								<div class="search-error" id="search-error"
									style="text-align: center"></div>
								<br>
								<div class="item item form-group">

									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="id" class="form-control col-md-7 col-xs-12"
											name="id" placeholder="" required="required" type="hidden">
									</div>
								</div>
								<div class="item item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="title">Title <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="title" class="form-control col-md-7 col-xs-12"
											name="title" placeholder="" required="required"
											type="text">
									</div>
								</div>
								<div class="item item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="description">Description <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="description" class="form-control col-md-7 col-xs-12"
											name="description" placeholder="" required="required"
											type="text">
									</div>
								</div>
								<div class="item form-group">
									<label for="meetingTime"
										class="control-label col-md-3 col-sm-3 col-xs-12">Meeting Time <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="meetingTime" class="form-control col-md-7 col-xs-12"
											type="text" name="meetingTime">
									</div>
								</div>

								
								<div class="item form-group">
									<label  for="user" class="control-label col-md-3 col-sm-3 col-xs-12">User
										<span class="">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="user"
											class=" form-control col-md-7 col-xs-12"
											type="text" name="user" required>
									</div>
								</div>
								<div class="item form-group">
									<label for="leadId"
										class="control-label col-md-3 col-sm-3 col-xs-12">Lead Id<span class="">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="leadId"
											class="form-control col-md-7 col-xs-12" type="text"
											name="leadId" required>
									</div>
								</div>
								<div class="item form-group">
									<label for="address"
										class="control-label col-md-3 col-sm-3 col-xs-12">Address <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="address" class="form-control col-md-7 col-xs-12"
											type="text" name="location" required>
									</div>
								</div>
								<div class="item form-group">
									<label  for="meetingTime" class="control-label col-md-3 col-sm-3 col-xs-12">Meeting Timing
										 <span class="">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="meetingTime"
											class="date-picker form-control col-md-7 col-xs-12"
											type="text" name="meetingTime" required>
									</div>
								</div>
								<div class="item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12">Close Timing <span class="">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="closeTiming"
											class="date-picker form-control col-md-7 col-xs-12"
											type="text" name="closeTiming" required>
									</div>
								</div>
								
								<div class="item form-group">
									<label for="status"
										class="control-label col-md-3 col-sm-3 col-xs-12">Status <span
										class="">*</span></label>
									<div class="col-md-6 col-sm-6 col-xs-12" required>
										<select class="form-control col-md-7 col-xs-12" name="status"
											id="status" required>
											<option value="">Select</option>
											<option value="1">Active</option>
											<option value="2">InActive</option>
										</select>
									</div>
								</div>
								<div class="ln_solid"></div>
								<div class="item form-group">
									<div class="col-md-6 col-md-offset-3" align="center">

										<button id="updateMeeting" type="submit" class="btn btn-success">
											Update <span id="loader"
												style="position: relative; top: 10px; right: 10px;"><i
												class="fa fa-circle-o-notch fa-spin" style="font-size: 24px"></i></span>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>