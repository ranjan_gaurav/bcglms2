<body 
class="nav-md">
	<div class="container body">
		<div class="main_container">
			<div class="col-md-3 left_col">
				<!-- sidebar -->
          <?php $this->view("includes/side_menu"); ?>
        </div>
		 <?php $this->view("includes/top_nav"); ?>
				<!-- page content -->

			<div class="right_col" role="main">
				<div class="">
					<div class="page-title">
						<div class="title_right">
							<div
								class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
							</div>
						</div>
					</div>

					<div class="clearfix"></div>

					<div class="row">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="x_panel">
								<div class="x_title">
									<h2>View image</h2>
									<div class="clearfix"></div>
								</div>
								<button type="button" class="btn btn-info btn-lg"
									data-toggle="modal" data-target="#myModal">Upload Image</button>

								<!-- Modal -->
								<div class="modal fade bs-privilegeDetails-modal-lg" tabindex="-1" id="myModal"
				role="dialog" aria-hidden="true">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">

						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">
								<span aria-hidden="true">&times;</span>
							</button>
							<h4 class="modal-title" id="myModalLabel">select image</h4>
						</div>
						<div class="modal-body">
							<form method="post" id="image" class="form-horizontal form-label-left" enctype="multipart/form-data"
								novalidate>


								<div class="search-error" id="search-error"
									style="text-align: center"></div>
								<br>
                                          <div class="item form-group">
									
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input id="id" class="form-control col-md-7 col-xs-12"
											name="id" placeholder="" required="required" type="hidden">
									</div>
								</div>
								
								<div class="item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="name">Description <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input type="text"
													class="form-control col-md-7 col-xs-12" name="description" id="description" />
									</div>
								</div>
								<div class="item form-group">
									<label class="control-label col-md-3 col-sm-3 col-xs-12"
										for="name">Choose Files <span class="required">*</span>
									</label>
									<div class="col-md-6 col-sm-6 col-xs-12">
										<input class="form-control col-md-7 col-xs-12" name="fileToUpload" id="fileToUpload" 
											 required="required" type="file">
									</div>
								</div>
								<div class="ln_solid"></div>
								<div class="form-group">
									<div class="col-md-6 col-md-offset-3" align="center">

										<input id="submit" type="button" name="fileSubmit"
										 class="btn btn-success" value="UPLOAD" >
											<span id="loader"
												style="position: relative; top: 10px; right: 10px;"><i
												class="fa fa-circle-o-notch fa-spin" style="font-size: 24px"></i></span>
										</button>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
								<div class="row">
									 
                    <?php      if(!empty($files)): foreach($files as $file): ?>
                   
                    <div class="col-sm-3">
										<img width="100"
											src="<?php echo base_url('uploads/files/'.$file->picture); ?>"
											
											alt="" class="img-responsive">
											 <?php echo $file->description; ?>
									</div>
											
                    <?php endforeach; ?>
                   
                    <?php else: ?>
                   
                    <p>File(s) not found.....</p>
                    <?php endif; ?>
									</div>

								<p><?php echo $links; ?></p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script>
	
	</script>

</body>