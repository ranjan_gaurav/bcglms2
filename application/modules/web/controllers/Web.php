<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Web extends CI_Controller {
	
	public function __construct() {
		parent::__construct();
		include_once './application/objects/Response.php';
		$this->load->helper('string');
		$this->load->library('form_validation');
	}
	
	public function index()
	{
			$this->load->view('web/home');
	}
	
	
}
